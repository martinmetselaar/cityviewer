//
//  MapWireframe.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import UIKit

class MapWireframe: MapWireframeProtocol {
	
	class func createMapModule(city: City) -> UIViewController {
		let view = MapView()
		let presenter: MapPresenterProtocol = MapPresenter()
		
		view.presenter = presenter
		presenter.view = view
		presenter.city = city
				
		return view
	}
}
