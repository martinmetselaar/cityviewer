//
//  MapPresenter.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import Foundation

class MapPresenter: MapPresenterProtocol {
	weak var view: MapViewProtocol?
	var wireframe: MapWireframeProtocol?
	var city: City?
	
	func viewDidLoad() {
		view?.showTitle(title: city!.name)
		
		view?.showLocation(latitude: city!.location.latitude, longitude: city!.location.longitude)
	}
}
