//
//  MapProtocols.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import UIKit

protocol MapViewProtocol: class {
	var presenter: MapPresenterProtocol? { get set }
	
	// Presenter to View
	func showTitle(title: String)
	func showLocation(latitude: Double, longitude: Double)
}

protocol MapWireframeProtocol: class {
	static func createMapModule(city: City) -> UIViewController
}

protocol MapPresenterProtocol: class {
	var view: MapViewProtocol? { get set }
	var wireframe: MapWireframeProtocol? { get set }
	var city: City? { get set }
	
	// View to Presenter
	func viewDidLoad()
}
