//
//  MapView.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import UIKit
import MapKit

class MapView: UIViewController {
	var presenter: MapPresenterProtocol?
	
	@IBOutlet weak var mapView: MKMapView!
	
	var data: City?
	
	override func viewDidLoad() {
		super.viewDidLoad()
				
		presenter?.viewDidLoad()
	}
}

//MARK: - SearchViewProtocol
extension MapView: MapViewProtocol {
	func showTitle(title: String) {
		self.title = title
	}
	
	func showLocation(latitude: Double, longitude: Double) {
		// Create Annotation
		let coordinate = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
		let annotation = MKPointAnnotation()
		annotation.coordinate = coordinate
		
		// Add Annotation to the map
		mapView.addAnnotation(annotation)
		
		// Center around the coordinates of the annotation
		mapView.setCenter(coordinate, animated: false)
	}
}
