//
//  ConvertDataManager.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import Foundation

class ConvertDataManager: ConvertDataManagerInputProtocol {
	var responseHandler: ConvertDataManagerOutputProtocol?
	
	//MARK: - Start
	
	func start() {
		start(fileName: "cities")
	}
	
	func start(fileName: String) {
		
		print("Start processing")
		var processedCities: [City]?
		do {
			processedCities = try process(fileName: fileName)
		} catch let error {
			responseHandler?.onCitiesError(error as! ProcessError)
			return
		}
		print("Done processing")
		
		print("Start sorting")
		let data = sort(cities: processedCities!)
		print("Done sorting")
		
		responseHandler?.onCities(data)
	}
	
	//MARK: - Sort
	
	func sort(cities: [City]) -> [City] {
		return cities.sorted { first, second in
			// First we should check if the names aren't the same
			if first.name != second.name {
				return first.name < second.name
			} else {
				// But when they are we should further filter on country
				return first.country < second.country
			}
		}
	}
	
	//MARK: - Process
	
	// Process based on fileName
	func process(fileName: String) throws -> [City] {
		// Retrieve the json file
		if let file = Bundle.main.url(forResource: fileName, withExtension: "json") {
			return try process(file: file)
		} else {
			print("Resource cities.json cannot be found")
			throw ProcessError.fileNotFound
		}
	}
	
	func process(file: URL) throws -> [City] {
		do {
			let jsonData = try Data(contentsOf: file)
			return try process(data: jsonData)
		} catch {
			print("Resource cities.json cannot be converted to Data")
			throw ProcessError.cannotConvertToData
		}
	}
	
	func process(data: Data) throws -> [City] {
		do {
			// Decode the Data into models using the JSONDecoder
			let cities = try JSONDecoder().decode([City].self, from: data)
			return cities
		} catch {
			print("Resource cities.json has a parsing error")
			throw ProcessError.parsing
		}
	}
}
