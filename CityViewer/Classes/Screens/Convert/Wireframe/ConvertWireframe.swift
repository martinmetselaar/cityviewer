//
//  ConvertWireframe.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import UIKit

class ConvertWireframe: ConvertWireframeProtocol {
	
	unowned var navigationController: UINavigationController
	
	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
	}
	
	class func createConvertModule(navController: UINavigationController) -> UIViewController {
		let view = ConvertView()
		let presenter: ConvertPresenterProtocol & ConvertInteractorOutputProtocol = ConvertPresenter()
		let interactor: ConvertInteractorInputProtocol & ConvertDataManagerOutputProtocol = ConvertInteractor()
		let dataManager: ConvertDataManagerInputProtocol = ConvertDataManager()
		let wireframe: ConvertWireframeProtocol = ConvertWireframe(navigationController: navController)
		
		view.presenter = presenter
		presenter.interactor = interactor
		presenter.view = view
		presenter.wireframe = wireframe
		
		interactor.presenter = presenter
		interactor.dataManager = dataManager
		
		dataManager.responseHandler = interactor
		
		return view
	}
	
	func showSearch(cities: [City]) {
		let controller = SearchWireframe.createSearchModule(navController: navigationController, cities: cities)
		navigationController.setViewControllers([controller], animated: true)
	}
}
