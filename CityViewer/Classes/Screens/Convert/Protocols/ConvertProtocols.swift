//
//  ConvertProtocols.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import UIKit

protocol ConvertViewProtocol: class {
	var presenter: ConvertPresenterProtocol? { get set }
	
	// Presenter to View
//	func showLoading()
//	func hideLoading()
	
	func showFileNotFoundError()
	func showCannotConvertDataError()
	func showParsingError()
}

protocol ConvertWireframeProtocol: class {
	static func createConvertModule(navController: UINavigationController) -> UIViewController
	
	func showSearch(cities: [City])
}

protocol ConvertPresenterProtocol: class {
	var view: ConvertViewProtocol? { get set }
	var interactor: ConvertInteractorInputProtocol? { get set }
	var wireframe: ConvertWireframeProtocol? { get set }

	// View to Presenter
	func viewDidLoad()
}

enum ProcessError: Error {
	case fileNotFound
	case cannotConvertToData
	case parsing
}

protocol ConvertDataManagerInputProtocol: class {
	var responseHandler: ConvertDataManagerOutputProtocol? { get set }
	
	// Interactor to DataManager
	func start()
}

protocol ConvertDataManagerOutputProtocol: class {
	// DataManager to Interactor
	func onCities(_ cities: [City])
	func onCitiesError(_ error: ProcessError)
}

protocol ConvertInteractorOutputProtocol: class {
	// Interactor to Presenter
	func onCities(_ cities: [City])
	func onCitiesError(_ error: ProcessError)
}

protocol ConvertInteractorInputProtocol: class {
	var presenter: ConvertInteractorOutputProtocol? { get set }
	var dataManager: ConvertDataManagerInputProtocol? { get set }
	
	// Presenter to Interactor
	func start()
}

