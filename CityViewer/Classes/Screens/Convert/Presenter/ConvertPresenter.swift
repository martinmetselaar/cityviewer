//
//  ConvertPresenter.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import Foundation

class ConvertPresenter: ConvertPresenterProtocol {
	weak var view: ConvertViewProtocol?
	var interactor: ConvertInteractorInputProtocol?
	var wireframe: ConvertWireframeProtocol?
	
	var data: [City] = []
	
	func viewDidLoad() {		
		interactor?.start()
	}
}

extension ConvertPresenter: ConvertInteractorOutputProtocol {
	
	func onCities(_ cities: [City]) {
		wireframe?.showSearch(cities: cities)
	}
	
	func onCitiesError(_ error: ProcessError) {
		
		// Show the correct error based on the error that is returned
		switch error {
		case .cannotConvertToData:
			view?.showCannotConvertDataError()
			break
		case .fileNotFound:
			view?.showFileNotFoundError()
			break
		case .parsing:
			view?.showParsingError()
			break
			
		}
	}
}
