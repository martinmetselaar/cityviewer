//
//  ConvertInteractor.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import Foundation

//MARK: - InputProtocol
class ConvertInteractor: ConvertInteractorInputProtocol {
	weak var presenter: ConvertInteractorOutputProtocol?
	var dataManager: ConvertDataManagerInputProtocol?
	
	func start() {
		// Excute search on a background thread
		DispatchQueue.global(qos: .background).async {
			self.dataManager?.start()
		}
	}
}

//MARK: - OutputProtocol
extension ConvertInteractor: ConvertDataManagerOutputProtocol {
	func onCities(_ cities: [City]) {
		// Background thread to Main thread
		DispatchQueue.main.async {
			self.presenter?.onCities(cities)
		}
	}
	
	func onCitiesError(_ error: ProcessError) {
		// Background thread to Main thread
		DispatchQueue.main.async {
			self.presenter?.onCitiesError(error)
		}
	}
}
