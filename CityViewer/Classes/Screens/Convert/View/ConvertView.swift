//
//  ConvertView.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import UIKit

class ConvertView: UIViewController {
	
	var presenter: ConvertPresenterProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
		
		presenter?.viewDidLoad()
    }

	func showError(message: String) {
		let title = NSLocalizedString("SearchViewErrorTitle", comment: "")
		let message = NSLocalizedString(message, comment: "")
		let confirm = NSLocalizedString("SearchViewErrorConfirm", comment: "")
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
		alert.addAction(UIAlertAction(title: confirm, style: UIAlertActionStyle.default, handler: nil))
		
		self.present(alert, animated: true, completion: nil)
	}
}

//MARK: - ConvertViewProtocol
extension ConvertView: ConvertViewProtocol {
	
	func showParsingError() {
		showError(message: "SearchViewErrorMessageParsing")
	}
	
	func showFileNotFoundError() {
		showError(message: "SearchViewErrorMessageFile")
	}
	
	func showCannotConvertDataError() {
		showError(message: "SearchViewErrorMessageData")
	}
	
}
