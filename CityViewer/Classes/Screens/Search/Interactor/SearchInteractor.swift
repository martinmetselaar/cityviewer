//
//  SearchInteractor.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import Foundation

//MARK: - InputProtocol
class SearchInteractor: SearchInteractorInputProtocol {
	weak var presenter: SearchInteractorOutputProtocol?
	var dataManager: SearchDataManagerInputProtocol?
	
	func search(_ query: String) {
		// Excute search on a background thread
		DispatchQueue.global(qos: .background).async {
			self.dataManager?.search(query)
		}
	}
}

//MARK: - OutputProtocol
extension SearchInteractor: SearchDataManagerOutputProtocol {
	func onCitiesFound(_ cities: [City]) {
		// We received the data on a background thread so we should execute this on the main thread
		DispatchQueue.main.async {
			self.presenter?.onCitiesFound(cities)
		}
	}
}
