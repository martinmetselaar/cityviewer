//
//  SearchView.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import UIKit

class SearchView: UIViewController {
	
	@IBOutlet weak var tableView: UITableView!
	@IBOutlet weak var searchBar: UISearchBar!
	
	var presenter: SearchPresenterProtocol?
	
	var data: [City] = []
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.title = NSLocalizedString("SearchViewTitle", comment: "")
		
		presenter?.viewDidLoad()
		
		tableView.register(UITableViewCell.self, forCellReuseIdentifier: "UITableViewCell")
		
		tableView.estimatedRowHeight = 44
		tableView.rowHeight = UITableViewAutomaticDimension
		tableView.tableFooterView = UIView()
		
		searchBar.placeholder = NSLocalizedString("SearchViewHint", comment: "")
	}
}

//MARK: - SearchViewProtocol
extension SearchView: SearchViewProtocol {
	func showCities(_ cities: [City]) {
		print("Show number of cities: \(cities.count)")
		
		data = cities
		tableView.reloadData()
	}
	
	func showLoading() {
		searchBar.isLoading = true
		
		// Reset the data when loading is displayed
		data = []
		tableView.reloadData()
	}
	
	func hideLoading() {
		searchBar.isLoading = false
	}
}

//MARK: - UITableViewDataSource, UITableViewDelegate
extension SearchView: UITableViewDataSource, UITableViewDelegate {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return data.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "UITableViewCell", for: indexPath)
		
		let city = data[indexPath.row]
		cell.textLabel?.text = String(format: NSLocalizedString("SearchViewCellFormat", comment: ""), city.name, city.country)
		
		return cell
	}
}

//MARK: - UISearchBarDelegate
extension SearchView: UISearchBarDelegate {
	func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
		print("Search for: \(searchText)")
		
		presenter?.didChangeSearchValue(searchText)
	}
	
	func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
		guard let searchText = searchBar.text, !searchText.isEmpty else {
			print("Nothing to search for")
			return
		}
		
		print("Search for: \(searchText)")
		presenter?.didEnterSearchValue(searchText)
	}
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		presenter?.didSelectIndex(index: indexPath.row)
		
		// Deselect the row for when the user returns
		tableView.deselectRow(at: indexPath, animated: true)
	}
}
