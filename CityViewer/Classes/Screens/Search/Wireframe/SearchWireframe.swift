//
//  SearchWireframe.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import UIKit

class SearchWireframe: SearchWireframeProtocol {
	
	unowned var navigationController: UINavigationController
	
	init(navigationController: UINavigationController) {
		self.navigationController = navigationController
	}
	
	class func createSearchModule(navController: UINavigationController, cities: [City]) -> UIViewController {
		let view = SearchView()
		let presenter: SearchPresenterProtocol & SearchInteractorOutputProtocol = SearchPresenter()
		let interactor: SearchInteractorInputProtocol & SearchDataManagerOutputProtocol = SearchInteractor()
		let dataManager: SearchDataManagerInputProtocol = SearchDataManager(data: cities)
		let wireframe: SearchWireframeProtocol = SearchWireframe(navigationController: navController)
		
		view.presenter = presenter
		presenter.interactor = interactor
		presenter.view = view
		presenter.wireframe = wireframe
		
		interactor.presenter = presenter
		interactor.dataManager = dataManager
		
		dataManager.responseHandler = interactor
		
		return view
	}
	
	func showMap(city: City) {
		let controller = MapWireframe.createMapModule(city: city)
		navigationController.pushViewController(controller, animated: true)
	}
}
