//
//  SearchProtocols.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import UIKit

protocol SearchViewProtocol: class {
	var presenter: SearchPresenterProtocol? { get set }
	
	// Presenter to View
	func showLoading()
	func hideLoading()
	
	func showCities(_ cities: [City])
}

protocol SearchWireframeProtocol: class {
	static func createSearchModule(navController: UINavigationController, cities: [City]) -> UIViewController
	
	func showMap(city: City)
}

protocol SearchPresenterProtocol: class {
	var view: SearchViewProtocol? { get set }
	var interactor: SearchInteractorInputProtocol? { get set }
	var wireframe: SearchWireframeProtocol? { get set }

	// View to Presenter
	func viewDidLoad()
	
	func didEnterSearchValue(_ searchValue: String)
	func didChangeSearchValue(_ searchValue: String)
	
	func didSelectIndex(index: Int)
}

protocol SearchDataManagerInputProtocol: class {
	var responseHandler: SearchDataManagerOutputProtocol? { get set }
	
	// Interactor to DataManager
	func search(_ query: String)
}

protocol SearchDataManagerOutputProtocol: class {
	// DataManager to Interactor
	func onCitiesFound(_ cities: [City])
}

protocol SearchInteractorOutputProtocol: class {
	// Interactor to Presenter
	func onCitiesFound(_ cities: [City])
}

protocol SearchInteractorInputProtocol: class {
	var presenter: SearchInteractorOutputProtocol? { get set }
	var dataManager: SearchDataManagerInputProtocol? { get set }
	
	// Presenter to Interactor
	func search(_ query: String)
}

