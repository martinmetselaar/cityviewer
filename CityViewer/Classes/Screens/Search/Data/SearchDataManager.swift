//
//  SearchDataManager.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import Foundation

class SearchDataManager: SearchDataManagerInputProtocol {
	var responseHandler: SearchDataManagerOutputProtocol?
	
	let data: [City]
	
	init(data: [City]) {
		self.data = data
	}
	
	//MARK: - Search
	
	func search(_ query: String) {
		search(query, cities: data)
	}
	
	func search(_ query: String, cities: [City]) {
		print("Start filtering")
		let filteredCities = filter(cities: cities, query: query)
		print("Done filtering")
		
		// Communicate the result
		responseHandler?.onCitiesFound(filteredCities)
	}
	
	//MARK: - Filter
	
	func filter(query: String) -> [City] {
		return filter(cities: data, query: query)
	}
	
	func filter(cities: [City], query: String) -> [City] {
		// Since searching should be case sensitive
		let queryLowercased = query.lowercased()
		
		return cities.filter { city in
			city.name.lowercased().starts(with: queryLowercased)
		}
	}
}
