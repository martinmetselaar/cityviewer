//
//  SearchPresenter.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import Foundation

class SearchPresenter: SearchPresenterProtocol {
	weak var view: SearchViewProtocol?
	var interactor: SearchInteractorInputProtocol?
	var wireframe: SearchWireframeProtocol?
	
	var searchTimer: Timer?
	var pendingSearchValue: String?
	
	var data: [City] = []
	
	func viewDidLoad() {
		// No initial action is required in this screen yet
	}
	
	func didChangeSearchValue(_ searchValue: String) {
		pendingSearchValue = searchValue
		
		// Cancel the previous timer
		searchTimer?.invalidate()
		// Schedule a new timer
		searchTimer = Timer.scheduledTimer(timeInterval: 0.6, target: self, selector: #selector(SearchPresenter.searchPending), userInfo: nil, repeats: false)
	}
	
	func didEnterSearchValue(_ searchValue: String) {
		// Cancel the timer and search directly
		searchTimer?.invalidate()
		
		search(searchValue)
	}
	
	@objc func searchPending() {
		search(pendingSearchValue ?? "")
	}
	
	func search(_ searchValue: String) {
		view?.showLoading()
		
		interactor?.search(searchValue)
	}
	
	func didSelectIndex(index: Int) {
		// Retrieve the model based on the index that was selected
		let city = data[index]
		
		// Route to the Map
		wireframe?.showMap(city: city)
	}
}

extension SearchPresenter: SearchInteractorOutputProtocol {
	func onCitiesFound(_ cities: [City]) {
		// Store the current visible data in the presenter
		self.data = cities
		
		// Show cities and hide loading since the loading is done
		view?.showCities(cities)
		view?.hideLoading()
	}
}
