//
//  City.swift
//  CityViewer
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import Foundation

/**
 * Since in the assignment it was described that it was not so important to optimize for fast searches I have chosen for readability.
 * Now other developers can use throughout the application this [City] model and it is clear what properties it has instead of for example a dictionary
*/
struct City: Codable {
	var name: String
	var country: String
	var location: Coordinate
	
	enum CodingKeys: String, CodingKey {
		case name
		case country
		case location = "coord"
	}
}

struct Coordinate: Codable {
	var latitude: Double
	var longitude: Double
	
	enum CodingKeys: String, CodingKey {
		case latitude = "lat"
		case longitude = "lon"
	}
}
