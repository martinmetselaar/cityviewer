//
//  SearchDataManagerTests.swift
//  CityViewerTests
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import XCTest
@testable import CityViewer

class SearchDataManagerTests: XCTestCase {
	
	var convertDataManager: ConvertDataManager!
	
    override func setUp() {
        super.setUp()
		
		convertDataManager = ConvertDataManager()
    }
    
    override func tearDown() {
		convertDataManager = nil
		
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
	
	func testFilterQueryA_shouldNotShowSydney() throws {
		let bundleFileReader = try BundleFileReader(fileName: "cities_requirements", ofType: "json")
		
		// First need to process
		let cities = try convertDataManager.process(file: bundleFileReader.fileURL)
		let searchDataManager = SearchDataManager(data: cities)
		
		// Filter
		let result = searchDataManager.filter(query: "A")
		
		XCTAssertEqual(result.count, 4, "There should only be 4 cities with the letter 'A'")
		
		result.forEach { (city: City) in
			XCTAssertNotEqual(city.name, "Sydney", "Sydney should not be one of the results")
		}
	}
	
	func testFilterQueryAl_shouldOnlyGiveAlabamaAndAlbuquerque() throws {
		let bundleFileReader = try BundleFileReader(fileName: "cities_requirements", ofType: "json")
		
		// First need to process
		let cities = try convertDataManager.process(file: bundleFileReader.fileURL)
		let searchDataManager = SearchDataManager(data: cities)

		// Filter
		let result = searchDataManager.filter(query: "Al")
		
		XCTAssertEqual(result.count, 2, "There should only be 2 cities with the letters 'Al'")
		XCTAssertEqual(result[0].name, "Alabama", "The first object should be 'Alabama' when searching for 'Al'")
		XCTAssertEqual(result[1].name, "Albuquerque", "The second object should be 'Albuquerque' when searching for 'Al'")
	}
	
	func testFilterQueryAlb_shouldOnlyGiveAlbuquerque() throws {
		let bundleFileReader = try BundleFileReader(fileName: "cities_requirements", ofType: "json")
		
		// First need to process
		let cities = try convertDataManager.process(file: bundleFileReader.fileURL)
		let searchDataManager = SearchDataManager(data: cities)

		// Filter
		let result = searchDataManager.filter(query: "Alb")
		
		XCTAssertEqual(result.count, 1, "There should only be 1 city with the letters 'Alb'")
		XCTAssertEqual(result[0].name, "Albuquerque", "The only object should be 'Alabama' when searching for 'Alb'")
	}
}
