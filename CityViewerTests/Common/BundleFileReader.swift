//
//  BundleFileReader.swift
//  CityViewerTests
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import Foundation

class BundleFileReader {
	let fileURL: URL
	
	init(fileName: String, ofType fileType: String) throws {
		let bundle = Bundle(for: type(of: self))
		let filePath = bundle.path(forResource: fileName, ofType: fileType)
		
		self.fileURL = URL.init(fileURLWithPath: filePath!)
	}
}


