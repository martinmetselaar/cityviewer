//
//  ConvertDataManagerTests.swift
//  CityViewerTests
//
//  Created by Martin Metselaar on 06/03/2018.
//  Copyright © 2018 Martin Metselaar. All rights reserved.
//

import XCTest
@testable import CityViewer

class ConvertDataManagerTests: XCTestCase {
	
	var dataManager: ConvertDataManager!
	
    override func setUp() {
        super.setUp()
		
		dataManager = ConvertDataManager()
    }
    
    override func tearDown() {
		dataManager = nil
		
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testProcess() throws {
		let bundleFileReader = try BundleFileReader(fileName: "cities_small", ofType: "json")
		
		// Start
		let cities = try dataManager.process(file: bundleFileReader.fileURL)
		
		XCTAssertNotNil(cities, "Data after processing is nil")
		XCTAssertEqual(cities.count, 29, "Data count after processing is not 29")
    }
	
	func testSortingCities_shouldBeOrdered() throws {
		let coordinate = Coordinate(latitude: 0.0, longitude: 0.0)
		
		// First need to create some unordered test data
		let cities: [City] = [
			City(name: "AAZZ", country: "AA", location: coordinate),
			City(name: "AZAA", country: "AA", location: coordinate),
			City(name: "ZZZZ", country: "AA", location: coordinate),
			City(name: "ZZZZ", country: "ZZ", location: coordinate),
			City(name: "AAAA", country: "ZZ", location: coordinate),
			City(name: "AAAA", country: "AA", location: coordinate)
		]
		
		// Created an array with the expected result by hand
		let expectedResult: [City] = [
			City(name: "AAAA", country: "AA", location: coordinate),
			City(name: "AAAA", country: "ZZ", location: coordinate),
			City(name: "AAZZ", country: "AA", location: coordinate),
			City(name: "AZAA", country: "AA", location: coordinate),
			City(name: "ZZZZ", country: "AA", location: coordinate),
			City(name: "ZZZZ", country: "ZZ", location: coordinate)
		]
		
		// Filter
		let result = dataManager.sort(cities: cities)
		
		for i in 0..<result.count {
			XCTAssertEqual(result[i].name, expectedResult[i].name, "Sorting went not so well, and failed on the name at index: \(i)")
			XCTAssertEqual(result[i].country, expectedResult[i].country, "Sorting went not so well, and failed on the country at index: \(i)")
		}
	}
}
